package pl.tomasz.smutek.recyclerviewproject;

import androidx.lifecycle.ViewModel;

import java.util.List;

public class MyViewModel extends ViewModel {

    MyRepository myRepository;

    public MyViewModel(){
        myRepository = new MyRepository();
    }

    public List<Quote> getAllQuotes(){
        return myRepository.getAllQuotes();
    }

    public Quote getQuote(int id){
        return myRepository.getQuote(id);
    }
}
