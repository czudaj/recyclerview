package pl.tomasz.smutek.recyclerviewproject;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditQuoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditQuoteFragment extends Fragment {


    public static final String TAG = "EditQuoteFragment";
    private static final String QUOTE_IT = "quote_id";

    private int mQuotedID;
    private Quote quote;

    public EditQuoteFragment() {
        // Required empty public constructor
    }


    public static EditQuoteFragment newInstance(int quoteID) {
        EditQuoteFragment fragment = new EditQuoteFragment();
        Bundle args = new Bundle();
        args.putInt(QUOTE_IT, quoteID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuotedID = getArguments().getInt(QUOTE_IT);
        }

        MyViewModel myViewModel = new ViewModelProvider(requireActivity()).get(MyViewModel.class);
        quote = myViewModel.getQuote(mQuotedID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_quote, container, false);
        EditText editText = view.findViewById(R.id.editTextTextPersonName);

        editText.setText(quote.getContent());

        return view;
    }
}