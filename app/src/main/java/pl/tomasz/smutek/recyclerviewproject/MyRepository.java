package pl.tomasz.smutek.recyclerviewproject;

import java.util.ArrayList;
import java.util.List;

public class MyRepository {

    List<Quote> quotes;

    public List<Quote> getAllQuotes() {
        if (quotes == null) {
            readFromDB();
        }
        return quotes;
    }

    private void readFromDB() {
        quotes = new ArrayList<>();
        quotes.add(new Quote("Nigdy wcześniej tak się nie działo.", 1));
        quotes.add(new Quote("Wczoraj działało.", 4));
        quotes.add(new Quote("Jak to możliwe?"));
        quotes.add(new Quote("To musi być problem sprzętowy."));
        quotes.add(new Quote("Co źle wpisałeś, że system padł?"));
        quotes.add(new Quote("Coś jest nie tak z twoimi danymi."));
        quotes.add(new Quote("Nie ruszałem tego kodu od tygodni!", 2));
        quotes.add(new Quote("Musisz mieć starą wersję."));
        quotes.add(new Quote("To musi być nieszczęśliwy zbieg okoliczności."));
        quotes.add(new Quote("Nie mogę przetestować wszystkiego!"));
        quotes.add(new Quote("TO nie może być przyczyna TEGO."));
        quotes.add(new Quote("Działa, ale nie było testowane."));
        quotes.add(new Quote("Ktoś musiał zmienić mój kod."));
        quotes.add(new Quote("Sprawdziłeś czy nie masz wirusa?"));
        quotes.add(new Quote("Nie działanew \"I jak się z tym czujesz?\""));
        quotes.add(new Quote("Nie możesz używać tej wersji na swoim systemie."));
        quotes.add(new Quote("Po co to robisz w ten sposób?"));
        quotes.add(new Quote("Gdzie byłeś kiedy pojawił się defekt?"));
        quotes.add(new Quote("Myślałem, że to naprawiłem."));
    }

    public Quote getQuote(int position) {
        return getAllQuotes().get(position);
    }
}
