package pl.tomasz.smutek.recyclerviewproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private final LayoutInflater layoutInflater;
    private final List<Quote> quotes;

    MyClickListener itemClickListener;
    MyClickListener scoreClickListener;

    interface MyClickListener {
        void onClick(View view, int position);
    }

    public MyRecyclerAdapter(Context context, List<Quote> quoteList) {
        layoutInflater = LayoutInflater.from(context);
        quotes = quoteList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.recycler_element, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.setItemClickListener(itemClickListener);
        viewHolder.setScoreClickListener(scoreClickListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Quote quote = quotes.get(position);
        holder.firstLine.setText(quote.getContent());

        if (quote.isScored()){
            holder.score.setText(Integer.toString(quote.getScore()));
        } else {
            holder.score.setText("");
        }

        holder.secondLine.setText("Jest to opis elementu numer: " + position);
    }

    @Override
    public int getItemCount() {
        return quotes.size();
    }

    public void setItemClickListener(MyClickListener clickListener) {
        this.itemClickListener = clickListener;
    }

    public void setScoreClickListener(MyClickListener scoreClickListener) {
        this.scoreClickListener = scoreClickListener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView firstLine;
        TextView secondLine;
        TextView score;
        ImageView makeScore;

        MyClickListener itemClickListener;
        MyClickListener scoreClickListener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onClick(v, getAdapterPosition());
                    }
                }
            });

            firstLine = itemView.findViewById(R.id.firsLine);
            secondLine = itemView.findViewById(R.id.secondLine);
            score = itemView.findViewById(R.id.score);
            makeScore = itemView.findViewById(R.id.make_score);

            makeScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (scoreClickListener != null){
                        scoreClickListener.onClick(v, getAdapterPosition());
                    }
                }
            });
        }

        public void setItemClickListener(MyClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        public void setScoreClickListener(MyClickListener scoreClickListener) {
            this.scoreClickListener = scoreClickListener;
        }
    }
}
