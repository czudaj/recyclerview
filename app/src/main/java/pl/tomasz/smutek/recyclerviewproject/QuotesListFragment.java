package pl.tomasz.smutek.recyclerviewproject;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link QuotesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuotesListFragment extends Fragment {

    public static final String TAG = "QuotesListFragment";
    private static final String ARG_PARAM1 = "param1";

    public interface ICommunicationInterface{
        void showActivityForQuote(int quotePosition);
        void showEditFragmentForQuote(int quotePosition);
    }

    private RecyclerView recyclerView;

    ICommunicationInterface communicationInterface;

    List<Quote> quoteList;

    private String mParam1;

    public QuotesListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment QuotesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuotesListFragment newInstance(String param1) {
        QuotesListFragment fragment = new QuotesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

        MyViewModel myViewModel = new ViewModelProvider(requireActivity()).get(MyViewModel.class);
        quoteList = myViewModel.getAllQuotes();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_quotes_list, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        MyRecyclerAdapter myRecyclerAdapter = new MyRecyclerAdapter(getContext(), quoteList);
        myRecyclerAdapter.setItemClickListener(new MyRecyclerAdapter.MyClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (communicationInterface != null){
                    communicationInterface.showActivityForQuote(position);
                }
            }
        });
        myRecyclerAdapter.setScoreClickListener(new MyRecyclerAdapter.MyClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (communicationInterface != null){
                    communicationInterface.showEditFragmentForQuote(position);
                }
            }
        });

        recyclerView.setAdapter(myRecyclerAdapter);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ICommunicationInterface){
            communicationInterface = (ICommunicationInterface) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        communicationInterface = null;
    }
}