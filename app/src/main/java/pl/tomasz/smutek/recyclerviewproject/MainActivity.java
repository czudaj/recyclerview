package pl.tomasz.smutek.recyclerviewproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements QuotesListFragment.ICommunicationInterface {

    private static final int MENU_ITEM_SHOW_TOAST_ID = 1;
    private static final int MENU_ITEM_SHOW_SNACKBAR_ID = 2;
    private MyViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(MyViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            putQuotesListFragment();
        }
    }

    private void putQuotesListFragment() {

//        QuotesListFragment quotesListFragment = new QuotesListFragment();  NIE!!!
        // Tworzymy fragment
        QuotesListFragment quotesListFragment = QuotesListFragment.newInstance("Cześć!");

        // Wrzucamy fragment do activity

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, quotesListFragment, QuotesListFragment.TAG);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);

        MenuItem menuItem = menu.add(Menu.NONE, MENU_ITEM_SHOW_TOAST_ID, Menu.NONE, R.string.menu_item_show_toast);
        menuItem.setIcon(android.R.drawable.ic_dialog_dialer);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menuItem = menu.add(Menu.NONE, MENU_ITEM_SHOW_SNACKBAR_ID, Menu.NONE, R.string.menu_item_show_snackbar);
        menuItem.setIcon(android.R.drawable.ic_dialog_email);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        boolean wasConsumed = super.onOptionsItemSelected(item);

        if (!wasConsumed) {
            switch (item.getItemId()) {

                case MENU_ITEM_SHOW_TOAST_ID:
                    Toast.makeText(this, R.string.menu_item_toast_clicked, Toast.LENGTH_SHORT).show();
                    break;

                case MENU_ITEM_SHOW_SNACKBAR_ID:

                    break;

                default:
                    break;
            }
        }

        return true;
    }

    @Override
    public void showActivityForQuote(int quotePosition) {
        Intent intent = new Intent(this, QuoteActivity.class);

        // TODO: brak dostępu do danych!!!
//        intent.putExtra(QuoteActivity.INTENT_CONTENT_QUOTE, quoteList.get(position).getContent());
        intent.putExtra(QuoteActivity.INTENT_CONTENT_QUOTE, "Brak danych");

        startActivity(intent);
    }

    @Override
    public void showEditFragmentForQuote(int quotePosition) {

        EditQuoteFragment quotesListFragment = EditQuoteFragment.newInstance(quotePosition);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, quotesListFragment, EditQuoteFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}