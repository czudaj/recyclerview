package pl.tomasz.smutek.recyclerviewproject;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

public class QuoteActivity extends AppCompatActivity {

    public static final String INTENT_CONTENT_QUOTE = "pl.tomasz.smutek.qoute";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);

        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(INTENT_CONTENT_QUOTE);

        TextView textView = findViewById(R.id.textView);
        textView.setText(stringExtra);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (savedInstanceState == null){
            // tutaj wedżie tylko podczas pierwszego tworzenia aktywności
        } else {
            // tutaj podczas "rekreacji" np. po obrocie.
        }

    }
}