package pl.tomasz.smutek.recyclerviewproject;

public class Quote {

    private String content;
    private int score = -1;

    public Quote(String content) {
        this.content = content;
    }

    public Quote(String content, int score) {
        this.content = content;
        this.score = score;
    }

    public String getContent() {
        return content;
    }

    public boolean isScored(){
        return score != -1;
    }

    public int getScore() {
        return score;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
